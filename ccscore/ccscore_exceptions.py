

class EmptyParagraph(Exception):
    """
    Raised when a paragraph didn't has sentences inside.
    """
    pass
